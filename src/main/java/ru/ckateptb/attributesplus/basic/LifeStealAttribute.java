package ru.ckateptb.attributesplus.basic;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.projectiles.ProjectileSource;
import ru.ckateptb.attributesplus.attributesplus.objects.Attributed;
import ru.ckateptb.attributesplus.basic.config.Config;

public class LifeStealAttribute extends BasicAttribute implements Listener {
    @Override
    public Listener getListener() {
        return this;
    }

    @Override
    public String getName() {
        return "lifesteal";
    }

    @Override
    public String getDisplay() {
        return Config.lifeSteal;
    }

    @Override
    public Boolean isVariable() {
        return true;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void on(EntityDamageByEntityEvent e) {
        Entity entityDamager = e.getDamager();
        LivingEntity damager = null;
        if (!e.getCause().equals(EntityDamageByEntityEvent.DamageCause.PROJECTILE))
            if (entityDamager instanceof LivingEntity)
                damager = (LivingEntity) entityDamager;
            else return;
        else if (entityDamager instanceof Arrow) {
            Arrow projectile = (Arrow) entityDamager;
            ProjectileSource shooter = projectile.getShooter();
            if (shooter instanceof LivingEntity)
                damager = (LivingEntity) shooter;
            else return;
        }
        if (damager == null) return;
        Attributed attributed = new Attributed(damager);
        Double value = attributed.get(getDisplay());
        Double damage = e.getDamage();
        Double health = damager.getHealth() + (damage * (value / 100));
        Double maxHealth = damager.getMaxHealth();
        if (health > maxHealth)
            health = maxHealth;
        damager.setHealth(health);
    }
}
