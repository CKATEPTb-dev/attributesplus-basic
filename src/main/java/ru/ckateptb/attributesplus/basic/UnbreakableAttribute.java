package ru.ckateptb.attributesplus.basic;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.ckateptb.attributesplus.basic.config.Config;

import java.util.List;

public class UnbreakableAttribute extends BasicAttribute implements Listener {
    @Override
    public Listener getListener() {
        return this;
    }

    @Override
    public String getName() {
        return "unbreakable";
    }

    @Override
    public String getDisplay() {
        return Config.unbreakable;
    }

    @Override
    public Boolean isVariable() {
        return false;
    }

    @EventHandler
    public void on(PlayerItemDamageEvent e) {
        ItemStack item = e.getItem();
        if (!item.hasItemMeta())
            return;
        ItemMeta meta = item.getItemMeta();
        if (!meta.hasLore())
            return;
        List<String> lore = meta.getLore();
        lore.forEach(line -> {
            if (line.equals(getDisplay())) {
                e.setCancelled(true);
                return;
            }
        });
    }
}
