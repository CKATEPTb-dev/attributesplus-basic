package ru.ckateptb.attributesplus.basic.config;

import ru.ckateptb.attributesplus.attributesplus.AttributesPlus;
import ru.ckateptb.tableapi.configs.Configurable;

import java.io.File;

@Configurable.ConfigFile
public class Config extends Configurable {
    @ConfigField(name = "attributes.lifeSteal.display")
    public static String lifeSteal = "§4§lLifeSteal: ";
    @ConfigField(name = "attributes.dodge.display")
    public static String dodge = "§3§lDodge: ";
    @ConfigField(name = "attributes.armor.display")
    public static String armor = "§a§lArmor: ";
    @ConfigField(name = "attributes.armor.enable", comment = "don't recommend")
    public static Boolean armorEnable = false;
    @ConfigField(name = "attributes.critical.display")
    public static String critical = "§e§lCritical: ";
    @ConfigField(name = "attributes.damage.display")
    public static String damage = "§7§lDamage: ";
    @ConfigField(name = "attributes.unbreakable.display")
    public static String unbreakable = "§2§lUnbreakable: ";
    @ConfigField(name = "attributes.health.display")
    public static String health = "§a§lHealth: ";
    @ConfigField(name = "attributes.defaultHealth", comment = "set 0 to disable")
    public static double defaultHealth = 20;

    public Config() {
        super(AttributesPlus.getInstance().getAttributesFolder() + File.separator + "basic.yml");
    }
}
