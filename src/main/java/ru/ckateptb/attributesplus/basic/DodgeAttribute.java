package ru.ckateptb.attributesplus.basic;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import ru.ckateptb.attributesplus.attributesplus.objects.Attributed;
import ru.ckateptb.attributesplus.basic.config.Config;

public class DodgeAttribute extends BasicAttribute implements Listener {
    @Override
    public Listener getListener() {
        return this;
    }

    @Override
    public String getName() {
        return "dodge";
    }

    @Override
    public String getDisplay() {
        return Config.dodge;
    }

    @Override
    public Boolean isVariable() {
        return true;
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void on(EntityDamageByEntityEvent e) {
        Entity ent = e.getEntity();
        if (!(ent instanceof LivingEntity))
            return;
        LivingEntity entity = (LivingEntity) ent;
        Attributed attributed = new Attributed(entity);
        Double value = attributed.get(getDisplay());
        if (Math.random() * 100 <= value)
            e.setCancelled(true);
    }
}
