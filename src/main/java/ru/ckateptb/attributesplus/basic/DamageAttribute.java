package ru.ckateptb.attributesplus.basic;

import net.minecraft.server.v1_12_R1.NBTTagCompound;
import net.minecraft.server.v1_12_R1.NBTTagInt;
import net.minecraft.server.v1_12_R1.NBTTagList;
import net.minecraft.server.v1_12_R1.NBTTagString;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;
import ru.ckateptb.attributesplus.attributesplus.objects.Attributed;
import ru.ckateptb.attributesplus.basic.config.Config;

public class DamageAttribute extends BasicAttribute implements Listener {
    @Override
    public Listener getListener() {
        return this;
    }

    @Override
    public String getName() {
        return "damage";
    }

    @Override
    public String getDisplay() {
        return Config.damage;
    }

    @Override
    public Boolean isVariable() {
        return true;
    }

    @Override
    public void after(Player player, ItemStack item) {
        net.minecraft.server.v1_12_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound compound = nmsStack.hasTag() ? nmsStack.getTag() : new NBTTagCompound();
        NBTTagList modifiers = compound.hasKey("AttributeModifiers")
                ? compound.getList("AttributeModifiers", 10)
                : new NBTTagList();
        NBTTagCompound damage = new NBTTagCompound();
        damage.set("AttributeName", new NBTTagString("generic.attackDamage"));
        damage.set("Name", new NBTTagString("generic.attackDamage"));
        damage.set("Amount", new NBTTagInt(0));
        damage.set("Operation", new NBTTagInt(0));
        damage.set("UUIDLeast", new NBTTagInt(894654));
        damage.set("UUIDMost", new NBTTagInt(2872));
        modifiers.add(damage);
        compound.set("AttributeModifiers", modifiers);
        nmsStack.setTag(compound);
        item = CraftItemStack.asBukkitCopy(nmsStack);
        super.after(player, item);
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void on(EntityDamageByEntityEvent e) {
        Entity entityDamager = e.getDamager();
        LivingEntity damager = null;
        if (!e.getCause().equals(EntityDamageByEntityEvent.DamageCause.PROJECTILE))
            if (entityDamager instanceof LivingEntity)
                damager = (LivingEntity) entityDamager;
            else return;
        else if (entityDamager instanceof Arrow) {
            Arrow projectile = (Arrow) entityDamager;
            ProjectileSource shooter = projectile.getShooter();
            if (shooter instanceof LivingEntity)
                damager = (LivingEntity) shooter;
            else return;
        }
        if (damager == null) return;
        Attributed attributed = new Attributed(damager);
        Double value = attributed.get(getDisplay());
        e.setDamage(e.getDamage() + value);
    }
}
