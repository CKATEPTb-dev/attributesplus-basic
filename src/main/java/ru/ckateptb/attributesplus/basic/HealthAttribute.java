package ru.ckateptb.attributesplus.basic;

import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import ru.ckateptb.attributesplus.attributesplus.objects.Attributed;
import ru.ckateptb.attributesplus.basic.config.Config;

public class HealthAttribute extends BasicAttribute {
    @Override
    public Listener getListener() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this.api, () -> {
            if (Config.defaultHealth == 0)
                return;
            for (Player player : Bukkit.getOnlinePlayers()) {
                Attributed attributed = new Attributed(player);
                AttributeInstance maxHealth = player.getAttribute(Attribute.GENERIC_MAX_HEALTH);
                double maxHealthValue = maxHealth.getValue();
                double amount = Config.defaultHealth + attributed.get(getDisplay());
                if (maxHealthValue != amount) {
                    double health = player.getHealth();
                    double multiplier = ((100 / maxHealthValue) * health) / 100;
                    maxHealth.setBaseValue(amount);
                    player.setHealth(amount * multiplier);
                }
            }
        }, 0, 20);
        return null;
    }

    @Override
    public String getName() {
        return "health";
    }

    @Override
    public String getDisplay() {
        return Config.health;
    }

    @Override
    public Boolean isVariable() {
        return true;
    }
}
