package ru.ckateptb.attributesplus.basic;

import net.minecraft.server.v1_12_R1.NBTTagCompound;
import net.minecraft.server.v1_12_R1.NBTTagInt;
import net.minecraft.server.v1_12_R1.NBTTagList;
import net.minecraft.server.v1_12_R1.NBTTagString;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.ckateptb.attributesplus.basic.config.Config;

import java.util.List;

public class ArmorAttribute extends BasicAttribute {
    @Override
    public Listener getListener() {
        return null;
    }

    @Override
    public void after(Player player, ItemStack item) {
        if (!Config.armorEnable) {
            player.sendMessage("§4Do u sex?");
            return;
        }
        int amount = 0;
        ItemMeta meta = item.getItemMeta();
        if (meta.hasLore()) {
            List<String> lore = meta.getLore();
            for (String line : lore) {
                if (line.contains(getDisplay()))
                    amount += Double.parseDouble(line.replace(getDisplay(), ""));
            }
        }
        net.minecraft.server.v1_12_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound compound = (nmsStack.hasTag()) ? nmsStack.getTag() : new NBTTagCompound();
        NBTTagList modifiers = new NBTTagList();
        NBTTagCompound toughness = new NBTTagCompound();
        toughness.set("AttributeName", new NBTTagString("generic.armorToughness"));
        toughness.set("Name", new NBTTagString("generic.armorToughness"));
        toughness.set("Amount", new NBTTagInt(amount));
        toughness.set("Operation", new NBTTagInt(0));
        toughness.set("UUIDLeast", new NBTTagInt(894654));
        toughness.set("UUIDMost", new NBTTagInt(2872));
        modifiers.add(toughness);
        NBTTagCompound armor = new NBTTagCompound();
        armor.set("AttributeName", new NBTTagString("generic.armor"));
        armor.set("Name", new NBTTagString("generic.armor"));
        armor.set("Amount", new NBTTagInt(amount));
        armor.set("Operation", new NBTTagInt(0));
        armor.set("UUIDLeast", new NBTTagInt(894654));
        armor.set("UUIDMost", new NBTTagInt(2872));
        modifiers.add(armor);
        compound.set("AttributeModifiers", modifiers);
        nmsStack.setTag(compound);
        item = CraftItemStack.asBukkitCopy(nmsStack);
        super.after(player, item);
    }

    @Override
    public String getName() {
        return "armor";
    }

    @Override
    public String getDisplay() {
        return Config.armor;
    }

    @Override
    public Boolean isVariable() {
        return true;
    }
}
