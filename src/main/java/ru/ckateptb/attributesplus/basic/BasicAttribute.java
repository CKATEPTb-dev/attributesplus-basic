package ru.ckateptb.attributesplus.basic;

import ru.ckateptb.attributesplus.attributesplus.objects.Attribute;
import ru.ckateptb.attributesplus.basic.config.Config;
import ru.ckateptb.tableapi.configs.Configurable;

public abstract class BasicAttribute extends Attribute {

    @Override
    public String getRequiredPlugin() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Basic attribute";
    }

    @Override
    public Configurable getConfig() {
        return new Config(); //todo
    }
}
